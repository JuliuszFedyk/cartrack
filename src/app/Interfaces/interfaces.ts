export interface Car {
  id: number;
  model: string;
  make: number;
  active: boolean;
  plates: string;
  euro?: string;
  location?: CarLocation;
}

export interface Driver {
  id: number;
  name: string;
}

export interface LoginData {
  email: string;
  password: string;
}

export interface CarLocation {
  id: number;
  time: number;
  lat: number;
  lon: number;
}
