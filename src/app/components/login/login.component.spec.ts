import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FakeAuthService } from './../../testing_utils/service-mocks';
import { AuthService } from './../../services/auth.service';
import { FormsModule } from '@angular/forms';
import { CustomMaterialModule } from './../../modules/custom-material/custom-material.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        CustomMaterialModule,
        FormsModule,
        RouterTestingModule
      ],
      declarations: [ LoginComponent ],
      providers: [
        {provide: AuthService, useClass: FakeAuthService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
