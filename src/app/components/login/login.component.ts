import { AuthService } from './../../services/auth.service';
import { LoginData } from './../../Interfaces/interfaces';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public data: LoginData;
  public loginFailed: boolean;
  constructor(private authService: AuthService, private router: Router) {
    this.loginFailed = false;
    this.data = {
      email: null,
      password: null
    };
  }
  loginRequest() {
    this.authService.loginRequest(this.data).then(() => {
      if (this.authService.isLoggedIn()) {
        this.loginFailed = false;
         return this.router.navigate(['']);
     } else {
        this.loginFailed = true;
     }
    });
  }
}
