import { Router } from '@angular/router';
import { CarsService } from './../../../services/cars.service';
import { Car } from './../../../Interfaces/interfaces';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carlist',
  templateUrl: './carlist.component.html',
  styleUrls: ['./carlist.component.scss']
})
export class CarlistComponent implements OnInit {
  public cars: Car[];

  constructor(private service: CarsService, private router: Router) { }

  ngOnInit() {
    this.service.carsSubject.subscribe(
      cars => this.cars = cars as Car[]
    );
  }
  onCarClick(id) {
    this.router.navigate(['cars', id]);
  }
}
