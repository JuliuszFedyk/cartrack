import { CarsService } from './../../../services/cars.service';
import { Car } from './../../../Interfaces/interfaces';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { find } from 'lodash';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {
  private cars: Car[];
  public car: Car;
  private id: number;

  constructor(private r: ActivatedRoute, private service: CarsService) {}

  ngOnInit() {
    this.r.params.subscribe(params => {
      this.id = parseInt(params.id, 10);
      this.update();
    });
    this.service.carsSubject.subscribe(cars => {
      this.cars = cars;
      this.update();
    });
  }

  update() {
    this.car = find(this.cars, {'id': this.id});
  }
}
