import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { CustomMaterialModule } from './modules/custom-material/custom-material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler/src/core';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CustomMaterialModule,
        HttpClientTestingModule
      ],
      declarations: [AppComponent, ToolbarComponent]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
