import { CarsService } from './services/cars.service';
import { LocationsService } from './services/locations.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { CustomMaterialModule } from './modules/custom-material/custom-material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CarsComponent } from './components/cars/cars.component';
import { CarlistComponent } from './components/cars/carlist/carlist.component';
import { MapComponent } from './components/cars/map/map.component';
import { CarComponent } from './components/cars/car/car.component';
import { LoginComponent } from './components/login/login.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CarsComponent,
    CarlistComponent,
    MapComponent,
    CarComponent,
    LoginComponent,
    ToolbarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CustomMaterialModule,
    FormsModule
  ],
  providers: [
    LocationsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
