import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoginData } from './../Interfaces/interfaces';
import { Injectable } from '@angular/core';
import { isMatch } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private logData: LogData;

  constructor(private http: HttpClient, private router: Router) {
    this.logData = JSON.parse(localStorage.getItem('logData')) || new LogData();
  }

  loginRequest(data: LoginData) {
    return this.http.get('/assets/data/login.json')
    .toPromise()
    .then(response => {
      if (isMatch(data, response)) {
        this.logData.email = data.email;
        this.logData.token = Math.random();
        localStorage.setItem('logData', JSON.stringify(this.logData));
      } else {
        this.logout();
      }
    });
  }
  logout() {
    this.logData.email = null;
    this.logData.token = null;
    localStorage.removeItem('logData');
    this.router.navigate(['/login']);
  }
  isLoggedIn() {
    return Boolean(this.logData.token);
  }
}

class LogData {
  constructor (
    public email: string = null,
    public token: number = null
  ) {}
}
