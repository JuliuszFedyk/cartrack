import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { TestBed, getTestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

import { AuthService } from './auth.service';

const loginData = {
  email: 'email@email.com',
  password: 'password'
};
let fakeHttp: HttpTestingController;
let service: AuthService;
let router: Router;

const routerSpy = {
  navigate: jasmine.createSpy('navigate')
};

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [AuthService, { provide: Router, useValue: routerSpy }]
    });
    fakeHttp = getTestBed().get(HttpTestingController);
    service = getTestBed().get(AuthService);
    router = getTestBed().get(Router);
  });

  afterEach(() => fakeHttp.verify());

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call http service, and log in', () => {
    service.loginRequest(loginData).then(() => {
      expect(service.isLoggedIn()).toBeTruthy();
    });
    const req = fakeHttp.expectOne('/assets/data/login.json');
    expect(req.request.method).toBe('GET');
    req.flush(loginData);
  });

  it('should not login with incorrect data', () => {
    const incorrectData = {
      email: 'dubdub@dub.dub',
      password: 'dubadubdub'
    };
    service.loginRequest(incorrectData).then(() => {
      expect(service.isLoggedIn()).toBeFalsy();
    });

    const req = fakeHttp.expectOne('/assets/data/login.json');
    expect(req.request.method).toBe('GET');
    req.flush(loginData);
  });
});
