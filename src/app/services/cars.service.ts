import { LocationsService } from './locations.service';
import { Car } from './../Interfaces/interfaces';
import { Injectable } from '@angular/core';
import { Subscription, ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { find } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class CarsService {
  private cars: Car[];
  private subscription: Subscription;
  public carsSubject: ReplaySubject<Array<Car>>;

  constructor(
    private http: HttpClient,
    private locationsService: LocationsService
  ) {
    this.carsSubject = new ReplaySubject();
    this.fetchCars();
  locationsService.locationsSubject.subscribe(location => {
      const car = find(this.cars, { id: location.id });
      car.location = location;
      this.publishCars();
    });
  }
  fetchCars() {
    this.subscription = this.http
      .get('/assets/data/cars.json')
      .subscribe(response => {
        this.cars = response as Car[];
        this.publishCars();
      });
  }
  publishCars() {
    this.carsSubject.next(this.cars);
  }
}
