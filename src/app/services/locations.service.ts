import { Subject, TimeInterval } from 'rxjs';
import { Injectable } from '@angular/core';
import { random } from 'lodash';
import { CarLocation } from '../Interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class LocationsService {
  public locationsSubject: Subject<CarLocation>;
  private locationTicker;

  constructor() {
    this.locationsSubject = new Subject();
    this.locationTicker = setInterval(this.issueTick.bind(this), 1000);
  }
  issueTick() {
    this.locationsSubject.next({
      id: random(1, 6) as number,
      time: Date.now(),
      lat: random(50, 54, true),
      lon: random(10, 16, true)
    });
  }
}
