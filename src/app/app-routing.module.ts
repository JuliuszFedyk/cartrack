import { MapComponent } from './components/cars/map/map.component';
import { LoginComponent } from './components/login/login.component';
import { CarComponent } from './components/cars/car/car.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarsComponent } from './components/cars/cars.component';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'cars',
    component: CarsComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: MapComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: ':id',
        component: CarComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'cars',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
