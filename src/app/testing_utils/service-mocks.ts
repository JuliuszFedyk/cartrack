import { Car } from './../Interfaces/interfaces';
import { ReplaySubject } from 'rxjs';
export class FakeAuthService {
  isLoggedIn() {
    return true;
  }
  logout() {}
}

export class FakeCarsService {
  public carsSubject: ReplaySubject<Array<Car>>;
  constructor() {
    this.carsSubject = new ReplaySubject();
    this.carsSubject.next([
      {
        id: 5,
        model: 'DAF FAR XF460',
        plates: 'REWR2233',
        euro: 'euro4',
        make: 2015,
        active: true
      }
    ]);
  }
}
